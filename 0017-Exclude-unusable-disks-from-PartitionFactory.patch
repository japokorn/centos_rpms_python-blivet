From f02dbed9143664246d400b0f5654062dff5383fc Mon Sep 17 00:00:00 2001
From: Vojtech Trefny <vtrefny@redhat.com>
Date: Thu, 13 Jan 2022 16:53:30 +0100
Subject: [PATCH 1/2] Exclude unusable disks from PartitionFactory

We already remove disks that are too small or not partitionable
in the PartitionSetFactory which allows us to create partitions
on multipath devices where Anaconda tells us to use both the mpath
device and the backing disks, we should do the same for the
PartitionFactory.

Resolves: rhbz#2017432
---
 blivet/devicefactory.py | 18 ++++++++++++++++++
 1 file changed, 18 insertions(+)

diff --git a/blivet/devicefactory.py b/blivet/devicefactory.py
index 0f7fdfa1..45b38b0f 100644
--- a/blivet/devicefactory.py
+++ b/blivet/devicefactory.py
@@ -1056,6 +1056,24 @@ class PartitionFactory(DeviceFactory):
                                             **kwargs)
         return device
 
+    def _configure(self):
+        disks = []
+        for disk in self.disks:
+            if not disk.partitioned:
+                log.debug("removing unpartitioned disk %s", disk.name)
+            elif not disk.format.supported:
+                log.debug("removing disk with unsupported format %s", disk.name)
+            else:
+                disks.append(disk)
+
+        if not disks:
+            raise DeviceFactoryError("no usable disks specified for partition")
+
+        log.debug("setting new factory disks to %s", [d.name for d in disks])
+        self.disks = disks  # pylint: disable=attribute-defined-outside-init
+
+        super(PartitionFactory, self)._configure()
+
     def _set_disks(self):
         self.raw_device.req_disks = self.disks[:]
 
-- 
2.34.1


From a9adcb050a16ab8231c81ced68302d6ad685ccf4 Mon Sep 17 00:00:00 2001
From: Vojtech Trefny <vtrefny@redhat.com>
Date: Thu, 13 Jan 2022 17:27:08 +0100
Subject: [PATCH 2/2] Show better error when using unitialized disk in
 do_partitioning

Now all we get is "KeyError: '/dev/sda'" for example.

Related: rhbz#2017432
---
 blivet/partitioning.py | 5 ++++-
 1 file changed, 4 insertions(+), 1 deletion(-)

diff --git a/blivet/partitioning.py b/blivet/partitioning.py
index 53f9cc3f..23b150f9 100644
--- a/blivet/partitioning.py
+++ b/blivet/partitioning.py
@@ -764,7 +764,10 @@ def allocate_partitions(storage, disks, partitions, freespace, boot_disk=None):
         growth = 0  # in sectors
         # loop through disks
         for _disk in req_disks:
-            disklabel = disklabels[_disk.path]
+            try:
+                disklabel = disklabels[_disk.path]
+            except KeyError:
+                raise PartitioningError("Requested disk %s doesn't have a usable disklabel for partitioning" % _disk.name)
             best = None
             current_free = free
             try:
-- 
2.34.1

